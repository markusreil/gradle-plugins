package com.mreil.gradleplugins.buildinfo

import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.internal.impldep.com.google.common.io.Files
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Shared
import spock.lang.Specification

import static com.mreil.gradleplugins.testutils.GradleTestUtils.runTask


@Slf4j
class BuildinfoPluginTest extends Specification {
    @Shared
    Project project

    def setup() {
        def dir = Files.createTempDir()
        log.info("Created temporary project dir: " + dir)
        project = ProjectBuilder.builder()
                .withName("testProject")
                .withProjectDir(dir)
                .build()
        project.plugins.apply("java")
        project.plugins.apply(BuildinfoPlugin)
    }

    def "Buildinfo is extracted"() {
        expect: "info is present"
        getExtension().info
    }

    def "File is generated"() {
        when: "task is run"
        runTask()

        then: "file exists"
        project.buildDir.toPath().resolve("buildinfo/buildinfo.json").toFile().exists()
    }

    private void runTask() {
        def name = BuildinfoTask.NAME
        runTask(project, name)
    }

    def "File generation is skipped"() {
        when: "generation is disabled"
        project.buildinfo.generate = false

        and: "task is run"
        runTask()

        then: "file does not exist"
        !project.buildDir.toPath().resolve("buildinfo/buildinfo.json").toFile().exists()
    }

    def "File is deleted after clean"() {
        when: "task is run"
        runTask()

        and: "clean is run"
        def clean = project.tasks.findByName("clean")
        clean.actions.each { a -> a.execute(clean) }

        then: "file does not exist"
        !project.buildDir.toPath().resolve("buildinfo/buildinfo.json").toFile().exists()
    }

    private BuildinfoContainerExtension getExtension() {
        project.extensions.getByName(BuildinfoContainerExtension.NAME) as BuildinfoContainerExtension
    }
}
