package com.mreil.gradleplugins.buildinfo.extractors

import org.eclipse.jgit.api.Git
import org.gradle.api.Project
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

class VcsPropertiesExtractorTest extends Specification {
    @Rule
    final TemporaryFolder testProjectDir = new TemporaryFolder()

    @Shared
    VcsPropertiesExtractor ex = new VcsPropertiesExtractor()

    Project project = Mock()

    def setup() {
        project.projectDir >> testProjectDir.root
    }

    def "Nothing is returned from non-git dir"() {
        when: "extract is called"
        def hash = ex.extract(project)

        then: "empty"
        hash == Optional.empty()
    }

    def "No commits on git repo"() {
        when: "Project dir is git repo without any commits"
        initRepo()

        and: "extract is called"
        def hash = ex.extract(project)

        then: "empty"
        hash == Optional.empty()
    }

    def "Hash is returned from git repo"() {
        when: "Project dir is git repo"
        Git git = initRepoAndCommit()

        and: "extract is called"
        def hash = ex.extract(project)

        then: "hash is returned"
        String commit = git.repository.findRef("HEAD").objectId.name()
        hash.get().getRevision() == commit
        hash.get().getShortRevision() == commit.substring(0, 6)
        hash.get().getBranch() == "master"
    }

    def "Branch is extracted"() {
        when: "Branch is checked out"
        Git git = initRepoAndCommit()
        git.branchCreate().setName("branch").call()
        git.checkout().setName("branch").call()

        and: "extract is called"
        def hash = ex.extract(project)

        then: "branch is returned"
        hash.get().getBranch() == "branch"
    }

    def "Tags are extracted"() {
        when: "Project has tags"
        Git git = initRepoAndCommit()
        String commit = git.repository.findRef("HEAD").objectId.name()
        ["tag1", "tag2"].forEach { tag ->
            git.tag().setName(tag).setAnnotated(false).call()
        }

        and: "extract is called"
        def vcsInfo = ex.extract(project)

        then: "tags are returned"
        vcsInfo.get().getTags() == ["tag1", "tag2"] as Set
        vcsInfo.get().getRevision() == commit
    }

    def "Unannotated tags are extracted from HEAD"() {
        when: "Project has tags"
        Git git = initRepoAndCommit()
        String commit = git.repository.findRef("HEAD").objectId.name()
        ["tag1", "tag2"].forEach { tag ->
            git.tag().setName(tag).setAnnotated(false).call()
        }
        git.checkout().setName("tag1").call()

        and: "extract is called"
        def vcsInfo = ex.extract(project)

        then: "tags are returned"
        vcsInfo.get().getTags() == ["tag1", "tag2"] as Set
        vcsInfo.get().getRevision() == commit
    }

    def "Unannotated tags are extracted after checkout"() {
        when: "Project has tags"
        Git git = initRepoAndCommit()
        String commit = git.repository.findRef("HEAD").objectId.name()
        ["tag1", "tag2"].forEach { tag ->
            git.tag().setName(tag).setAnnotated(false).call()
        }
        addFileAndCommit(git)
        git.checkout().setName("tag1").call()

        and: "extract is called"
        def vcsInfo = ex.extract(project)

        then: "tags are returned"
        vcsInfo.get().getTags() == ["tag1", "tag2"] as Set
        vcsInfo.get().getRevision() == commit
    }

    def "Annotated tags are extracted after checkout"() {
        when: "Project has tags"
        Git git = initRepoAndCommit()
        String commit = git.repository.findRef("HEAD").objectId.name()
        ["tag1", "tag2"].forEach { tag ->
            git.tag().setName(tag)
                    .setAnnotated(true)
                    .setMessage("setting tag " + tag)
                    .call()
        }
        addFileAndCommit(git)
        git.checkout().setName("tag1").call()

        and: "extract is called"
        def vcsInfo = ex.extract(project)

        then: "tags are returned"
        vcsInfo.get().getTags() == ["tag1", "tag2"] as Set
        vcsInfo.get().getRevision() == commit
    }

    def "Annotated tags are extracted from HEAD"() {
        when: "Project has tags"
        Git git = initRepoAndCommit()
        String commit = git.repository.findRef("HEAD").objectId.name()
        ["tag1", "tag2"].forEach { tag ->
            git.tag().setName(tag)
                    .setMessage("setting tag " + tag)
                    .setAnnotated(true)
                    .call()
        }

        and: "extract is called"
        def vcsInfo = ex.extract(project)

        then: "tags are returned"
        vcsInfo.get().getTags() == ["tag1", "tag2"] as Set
        vcsInfo.get().getRevision() == commit
    }

    private Git initRepoAndCommit() {
        Git git = initRepo()
        addFileAndCommit(git)
        git
    }

    private Git initRepo() {
        Git git = Git.init().setDirectory(testProjectDir.root).call()
        git
    }

    private void addFileAndCommit(Git git) {
        testProjectDir.newFile() << "meh"
        git.add().addFilepattern(".").call()
        git.commit().setMessage("mycommit").call()
    }
}
