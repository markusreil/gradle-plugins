package com.mreil.gradleplugins.buildinfo

import com.mreil.gradleplugins.buildinfo.extractors.VcsPropertiesExtractor
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import org.eclipse.jgit.api.Git
import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import java.util.zip.ZipFile

@Slf4j
class BuildinfoPluginFuncTest extends Specification {
    @Rule
    final TemporaryFolder testProjectDir = new TemporaryFolder()

    GradleRunner runner
    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile("build.gradle")
        runner = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withPluginClasspath()
    }

    def "Buildinfo file is generated"() {
        given: "plugin is applied"
        buildFile << """
            plugins {
                id 'com.mreil.buildinfo'
            }
        """

        when: "task is run"
        def result = runner
                .withArguments('buildinfo')
                .build()

        then: "task succeeds"
        result.task(":buildinfo").outcome == TaskOutcome.SUCCESS

        and: "buildinfo file exists"
        def outFile = runner.projectDir.toPath().resolve("build/buildinfo/buildinfo.json").toFile()
        outFile.exists()

        and: "buildinfo file has content"
        new JsonSlurper().parse(outFile)["dateTime"]
    }

    def "Vcsinfo has tags"() {
        given: "is git repo and has tags"
        // init git
        Git git = Git.init().setDirectory(testProjectDir.root).call()
        // commit file
        testProjectDir.newFile("file1") << "content1"
        git.add().addFilepattern("file1").call()
        git.commit().setMessage("commit file1").call()
        // add tags
        git.tag().setName("tag1").setAnnotated(false).call()
        git.tag().setName("tag2").setMessage("message for tag2").call()
        // commit other file
        testProjectDir.newFile("file2") << "content2"
        git.add().addFilepattern("file2").call()
        git.commit().setMessage("commit file2").call()
        // checkout tag
        git.checkout().setName("tag1").call()

        and: "plugin is applied"
        buildFile << """
            plugins {
                id 'com.mreil.buildinfo'
            }
        """

        when: "task is run"
        def result = runner
                .withArguments('buildinfo',  '--stacktrace')
                .build()

        then: "task succeeds"
        result.task(":buildinfo").outcome == TaskOutcome.SUCCESS

        and: "buildinfo file exists"
        def outFile = runner.projectDir.toPath().resolve("build/buildinfo/buildinfo.json").toFile()
        outFile.exists()

        and: "buildinfo file has content"
        def info = new JsonSlurper().parse(outFile)
        info
        info[VcsPropertiesExtractor.PROPERTY_KEY]
        def tags = info[VcsPropertiesExtractor.PROPERTY_KEY]["tags"]
        tags == ["tag1", "tag2"]
    }

    def "Buildinfo file is part of jar"() {
        given: "plugin is applied"
        testProjectDir.newFolder("src", "main", "java")
        testProjectDir.newFile("src/main/java/Hello.java") << """
            public class Hello {}
        """
        buildFile << """
            plugins {
                id 'com.mreil.buildinfo'
            }
            apply plugin: 'java'
        """

        when: "task is run"
        BuildResult result = runner
                .withArguments('build')
                .build()
        System.err.println result.output

        then: "task succeeds"
        result.task(":build").outcome == TaskOutcome.SUCCESS

        and: "jar exists"
        def outJar = testProjectDir.root.toPath().resolve("build/libs").toFile().listFiles()
                .find { file -> file.name.endsWith(".jar") }
        outJar.exists()

        and: "jar contains buildinfo"
        new ZipFile(outJar).entries().find{file ->
            file.name.equals("buildinfo.json")
        }
    }

}
