package com.mreil.gradleplugins.buildinfo.extractors

import org.gradle.api.Project
import spock.lang.Specification

class BuildNumberExtractorTest extends Specification {
    Project project = Mock()
    BuildNumberExtractor.Environment env = Mock()

    def "Extract"() {
        def extractor = new BuildNumberExtractor()
        env.getVariable("SEMAPHORE_BUILD_NUMBER") >> Optional.of("123")
        env.getVariable(_) >> Optional.empty()
        extractor.env = env

        when: "build number is extracted"
        def buildNumber = extractor.extract(project).get()

        then: "it is present"
        buildNumber == "123"
    }
}
