package com.mreil.gradleplugins.buildinfo.extractors

import org.gradle.api.Project
import spock.lang.Specification

import java.time.format.DateTimeFormatter

class DateTimeExtractorTest extends Specification {
    Project project = Mock()

    def "Extract"() {
        def extractor = new DateTimeExtractor()

        when: "date is extracted"
        def date = extractor.extract(project).get()

        then: "it is valid"
        date.length() == 12
        DateTimeFormatter.ofPattern("yyyyMMddHHmm").parse(date)
    }
}
