package com.mreil.gradleplugins.buildinfo.extractors;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.gradle.api.Project;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class VcsPropertiesExtractor implements InfoExtractor<VcsPropertiesExtractor.VcsProperties> {
    public static final String PROPERTY_KEY = "vcs";

    @Override
    public Optional<VcsProperties> extract(Project project) throws IOException {
        try {
            FileRepositoryBuilder builder = new FileRepositoryBuilder();

            Repository repository = builder
                    .setGitDir(new File(project.getProjectDir().toString() + "/.git"))
                    .readEnvironment() // scan environment GIT_* variables
                    .setMustExist(true)
                    .findGitDir() // scan up the file system tree
                    .build();

            Ref current = repository.findRef("HEAD");
            ObjectId commit = current.getObjectId();

            if(commit == null) {
                log.error("Buildinfo plugin cannot extract git commit info. Has there been a commit?");
                return Optional.empty();
            }

            return Optional.of(VcsProperties.builder()
                    .revision(commit.name())
                    .shortRevision(commit.abbreviate(6).name())
                    .tags(getTags(repository, commit))
                    .branch(repository.getBranch())
                    .build());
        } catch (RepositoryNotFoundException e) {
            log.info("This is not a git repo, not extracting commit hash.");
            log.debug(e.getMessage(), e);
            return Optional.empty();
        }
    }

    @Override
    public String getKey() {
        return PROPERTY_KEY;
    }

    private Set<String> getTags(Repository repository, ObjectId commit) {
        log.info("getting tags for commit: " + commit);
        Map<String, Ref> tags = repository.getTags();
        log.info("tags are: " + tags);
        return tags.entrySet().stream()
                .filter(e -> {
                            Ref ref = e.getValue();
                            log.info("testing ref: " + ref);
                            ObjectId peeledId = repository.peel(e.getValue()).getPeeledObjectId();
                            log.info("peeledId: " + peeledId);
                            return e.getValue().getObjectId().equals(commit) ||
                                    (peeledId != null && peeledId.equals(commit));
                        }
                )
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
    }

    @Data
    @Builder
    public static class VcsProperties {
        private String revision;
        private String shortRevision;
        private Set<String> tags;
        private String branch;
    }
}
