package com.mreil.gradleplugins.buildinfo

import groovy.json.JsonOutput
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.StandardOpenOption

/**
 * TODO
 */
class BuildinfoTask extends DefaultTask {
    static String NAME = "buildinfo"

    @TaskAction
    void doTask() {
        BuildinfoExtension info = findExtension()

        if (info.generate) {
            generateFile()
        }
    }

    private BuildinfoExtension findExtension() {
        project.extensions.getByType(BuildinfoExtension)
    }

    private void generateFile() {
        BuildinfoContainerExtension container = BuildinfoPlugin.getOrCreateExtension(project)
        outputFile().toPath().parent.toFile().mkdirs()
        Files.write(outputFile().toPath(),
                JsonOutput.toJson(container.info).getBytes(Charset.forName("UTF-8")),
                StandardOpenOption.WRITE,
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING)
    }

    @OutputFile
    File outputFile() {
        return findExtension().getOutputFile()
    }
}
