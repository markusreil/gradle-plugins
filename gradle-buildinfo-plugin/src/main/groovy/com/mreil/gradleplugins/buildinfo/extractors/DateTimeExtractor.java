package com.mreil.gradleplugins.buildinfo.extractors;

import org.gradle.api.Project;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

public class DateTimeExtractor implements InfoExtractor<String> {
    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
    public static final String PROPERTY_KEY = "dateTime";

    @Override
    public Optional<String> extract(Project project) {
        return Optional.of(dtf.format(LocalDateTime.now(ZoneOffset.UTC)));
    }

    @Override
    public String getKey() {
        return PROPERTY_KEY;
    }
}
