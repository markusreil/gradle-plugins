package com.mreil.gradleplugins.buildinfo.extractors;

import org.gradle.api.Project;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class BuildNumberExtractor implements InfoExtractor<String> {
    public static final String PROPERTY_KEY = "buildNumber";
    Environment env = new Environment();

    private final List<String> vars = Arrays.asList(
            "bamboo.buildNumber",
            "SEMAPHORE_BUILD_NUMBER",
            "CIRCLE_BUILD_NUM");

    @Override
    public Optional<String> extract(Project project) {
        return vars.stream()
                .map(env::getVariable)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .findFirst();
    }

    @Override
    public String getKey() {
        return PROPERTY_KEY;
    }

    public static class Environment {
        public Optional<String> getVariable(String var) {
            return Optional.ofNullable(System.getenv(var));
        }
    }
}
