package com.mreil.gradleplugins.buildinfo.extractors;

import org.gradle.api.Project;

import java.io.IOException;
import java.util.Optional;

public interface InfoExtractor<T> {
    Optional<T> extract(Project project) throws IOException;
    String getKey();
}
