package com.mreil.gradleplugins.buildinfo

import com.mreil.gradleplugins.buildinfo.extractors.BuildNumberExtractor
import com.mreil.gradleplugins.buildinfo.extractors.DateTimeExtractor
import com.mreil.gradleplugins.buildinfo.extractors.VcsPropertiesExtractor
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * TODO
 */
class BuildinfoPlugin implements Plugin<Project> {

    public static final String DEFAULT_FILE = "buildinfo/buildinfo.json"

    @Override
    void apply(Project project) {
        createBuildInfoAgainstRootProject(project)

        project.allprojects.each { p ->
            createTaskAndConfigForProject(p)
        }
    }

    static createTaskAndConfigForProject(Project project) {
        def config = project.extensions.create(BuildinfoExtension.NAME,
                BuildinfoExtension)

        config.outputFile = project.buildDir.toPath().resolve(DEFAULT_FILE).toFile()

        def task = project.tasks.create(BuildinfoTask.NAME, BuildinfoTask)

        project.afterEvaluate{
            def pr = project.tasks.findByName("processResources")
            if (pr!=null) {
                pr.dependsOn(task)
            }

            def dir = task.outputFile().toPath().parent.toFile()
            if(project.plugins.findPlugin("java")) {
                project.sourceSets.main.resources.srcDir dir
            }
        }

    }

    private static void createBuildInfoAgainstRootProject(Project project) {
        Project root = project.parent ?: project
        BuildinfoContainerExtension container = getOrCreateExtension(root)
        if (container.info == null) {
            container.info = extractInfo(root)
        }
    }

    static BuildinfoContainerExtension getOrCreateExtension(Project project) {
        Project root = project.parent ?: project
        root.extensions.findByName(BuildinfoContainerExtension.NAME) as BuildinfoContainerExtension ?:
                root.extensions.create(
                        BuildinfoContainerExtension.NAME,
                        BuildinfoContainerExtension)
    }

    private static Map<String, Object> extractInfo(Project project) {
        [
                new DateTimeExtractor(),
                new VcsPropertiesExtractor(),
                new BuildNumberExtractor()
        ]
                .collect { e -> [e.getKey(), e.extract(project)] }
                .findAll { k, v -> v.isPresent() }
                .collectEntries { k, v -> [k, v.get()] }
    }
}
