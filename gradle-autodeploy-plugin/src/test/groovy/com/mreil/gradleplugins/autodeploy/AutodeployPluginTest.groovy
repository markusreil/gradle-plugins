package com.mreil.gradleplugins.autodeploy

import com.mreil.gradleplugins.buildinfo.BuildinfoPlugin
import org.eclipse.jgit.api.Git
import org.gradle.api.Project
import org.gradle.api.ProjectConfigurationException
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

class AutodeployPluginTest extends Specification {
    @Rule
    final TemporaryFolder testProjectDir = new TemporaryFolder()

    @Shared
    Project project
    private Git git

    def setup() {
        project = ProjectBuilder.builder()
                .withProjectDir(testProjectDir.root)
                .build()
        project.version = "1.2.3"
        project.plugins.apply("java")
    }

    def "Plugin was applied"() {
        given: "plugin is applied"
        project.plugins.apply(AutodeployPlugin)

        expect: "plugin is in plugins"
        project.plugins.findPlugin(AutodeployPlugin)
    }

    def "Plugin dependency was applied"() {
        given: "plugin is applied"
        project.plugins.apply(AutodeployPlugin)

        expect: "plugin is in plugins"
        project.plugins.findPlugin(BuildinfoPlugin)
    }

    def "Missing deploy task throws"() {
        given: "is git project"
        initGit()
        tag("1.2.3")

        and: "plugin is applied"
        project.plugins.apply(AutodeployPlugin)

        when: "project is evaluated"
        project.evaluate()

        then: "error"
        ProjectConfigurationException ex = thrown()
        ex.cause.message == "Unknown deploy task: bintrayUpload"
    }

    def tag(String tag) {
        git.tag().setAnnotated(false).setName("release/test/v" + tag).call()
    }

    private void initGit() {
        git = Git.init().setDirectory(testProjectDir.root).call()
        git.add().addFilepattern(".").call()
        git.commit().setMessage("mycommit").call()
    }
}
