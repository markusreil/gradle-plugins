package com.mreil.gradleplugins.autodeploy

import com.mreil.gradleplugins.buildinfo.BuildinfoContainerExtension
import com.mreil.gradleplugins.buildinfo.BuildinfoPlugin
import com.mreil.gradleplugins.buildinfo.extractors.VcsPropertiesExtractor
import groovy.text.SimpleTemplateEngine
import groovy.util.logging.Slf4j
import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * TODO
 */
@Slf4j
class AutodeployPlugin implements Plugin<Project> {
    void apply(Project project) {
        // depends on BI plugin
        project.getPluginManager().apply(BuildinfoPlugin.class)

        AutodeployExtension ext =
                project.extensions.create(AutodeployExtension.NAME,
                        AutodeployExtension)

        AutodeployTask task = project.tasks.create(AutodeployTask.NAME, AutodeployTask)

        project.afterEvaluate {
            if (!shouldDeploy(project, ext)) {
                log.info("Skip autodeploy")
                return
            }

            def deployTask = project.tasks.findByName(ext.deployTask)
            if(!deployTask) {
                throw new GradleException("Unknown deploy task: " + ext.deployTask)
            }
            task.dependsOn(deployTask)
        }
    }

    static boolean shouldDeploy(Project project,
                                AutodeployExtension autodeployExtension) {

        def engine = new SimpleTemplateEngine()
        def binding = [
                "project": project
        ]
        def releaseTag = engine.createTemplate(autodeployExtension.releaseTagPattern)
                .make(binding).toString()

        log.info("Checking if current tag is: " + releaseTag)

        return currentVcsTagsContain(project, releaseTag)
    }

    static boolean currentVcsTagsContain(Project project, String tag) {
        BuildinfoContainerExtension ext =
                (project.parent ?: project)
                        .getExtensions().getByName(BuildinfoContainerExtension.NAME)

        def vcsProps = ext.info[VcsPropertiesExtractor.PROPERTY_KEY] as
                VcsPropertiesExtractor.VcsProperties

        if(!vcsProps) {
            log.error("No VCS properties in build info. Is this project under version control?")
            return false
        }
        def vcsTags = vcsProps.tags
        def contains = vcsTags.contains(tag)

        if (contains) {
            log.info("Tag template: " + tag + " is contained in current tags: " + vcsProps)
        } else {
            log.info("Tag template: " + tag + " is not contained in current tags: " + vcsProps)
        }

        return contains
    }
}
