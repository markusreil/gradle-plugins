package com.mreil.gradleplugins.autodeploy

import groovy.util.logging.Slf4j
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

@Slf4j
class AutodeployTask extends DefaultTask {
    public static String NAME = "autodeploy"

    @TaskAction
    doTask() {
    }
}
