package com.mreil.gradleplugins.autoversion

import com.github.zafarkhaja.semver.Version
import org.gradle.api.GradleException

class VersionPluginExtension {
    public static final String NAME = "autoversion"

    // Configuration properties
    File versionPropertyFile

    private Mode mode = Mode.RELEASE
    void mode(String mode) {
        try {
            this.mode = Mode.valueOf(mode.toUpperCase())
        } catch (IllegalArgumentException e) {
            throw new GradleException("Mode must be one of: " + Mode.values())
        }
    }
    String getMode() {
        return mode.toString()
    }

    // read-only properties, set by plugin
    protected boolean specified = false
    protected Optional<Version> semVer
    protected String originalVersion
    boolean isSnapshot() {
        return semVer.map { v -> v.getPreReleaseVersion() == "SNAPSHOT" }
                .orElse(false)
    }

    protected static enum Mode {
        RELEASE,
        CONTINUOUS
    }
}
