package com.mreil.gradleplugins.autoversion

import com.github.zafarkhaja.semver.ParseException
import com.github.zafarkhaja.semver.Version
import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project

/**
 * TODO
 */
class VersionPlugin implements Plugin<Project> {
    public static final String DEFAULT_PROPERTIES_FILE = "version.properties"
    public static final String DEFAULT_VERSION_PROPERTY = "version"

    @Override
    void apply(Project project) {
        def ext = project.extensions.create(VersionPluginExtension.NAME,
                VersionPluginExtension)

        project.tasks.create(VersionTask.NAME, VersionTask)

        project.afterEvaluate { p ->
            configureProject(p, ext)
        }
    }

    private static void configureProject(Project project, VersionPluginExtension ext) {
        setVersionPropertiesFileIfExists(project, ext)
        String version = getVersionFromPropertiesOrProject(project, ext)

        ext.originalVersion = version
        if (version != "unspecified") {
            ext.specified = true
        }
        try {
            Version.Builder builder = new Version.Builder(version)
            ext.semVer = Optional.of(builder.build())
        } catch (ParseException e) {
            ext.semVer = Optional.empty()
        }

        if(ext.mode == VersionPluginExtension.Mode.CONTINUOUS.toString()) {
            // TODO
        }
        project.version = version
    }

    static String getVersionFromPropertiesOrProject(Project project,
                                                    VersionPluginExtension ext) {
        def propertiesFile = ext.versionPropertyFile
        if (propertiesFile) {
            if (!propertiesFile.exists()) {
                throw new GradleException("File does not exist: " + propertiesFile)
            }
            def props = new Properties()
            propertiesFile.withInputStream {
                stream -> props.load(stream)
            }
            def versionFromProps = props.getProperty(DEFAULT_VERSION_PROPERTY)
            if (!versionFromProps) {
                throw new GradleException("File " + propertiesFile + " does not contain property: " +
                        DEFAULT_VERSION_PROPERTY)
            } else if (project.version != "unspecified"){
                throw new GradleException("Version cannot be specified in properties file and in project.")
            } else {
                return versionFromProps
            }
        } else {
            return project.version.toString()
        }
    }

    static setVersionPropertiesFileIfExists(Project project,
                                            VersionPluginExtension ext) {
        def propertiesFile = project.file(DEFAULT_PROPERTIES_FILE)
        if (propertiesFile.exists()) {
            ext.versionPropertyFile = propertiesFile
        }
    }
}
