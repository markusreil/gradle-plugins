package com.mreil.gradleplugins.autoversion

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * TODO
 */
class VersionTask extends DefaultTask {
    static String NAME = "version"

    @TaskAction
    void doTask() {
        VersionPluginExtension ext =
                project.extensions.getByName(VersionPluginExtension.NAME) as VersionPluginExtension
        project.logger.error(ext.toString())
    }
}
