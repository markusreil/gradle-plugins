package com.mreil.gradleplugins.autoversion

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

/**
 * TODO
 */
class VersionPluginFuncTest extends Specification {
    @Rule
    final TemporaryFolder testProjectDir = new TemporaryFolder()

    GradleRunner runner
    File buildFile

    def setup() {
        buildFile = testProjectDir.newFile("build.gradle")
        runner = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withPluginClasspath()
    }

    def "Jar has version"() {
        given: "version properties file is present"
        testProjectDir.newFile("version.properties") << """
            version=1.2.3
        """

        and: "plugin is applied to java project"
        testProjectDir.newFolder("src", "main", "java")
        testProjectDir.newFile("src/main/java/Hello.java") << """
            public class Hello {}
        """
        buildFile << """
            plugins {
                id 'com.mreil.autoversion'
            }
            apply plugin: 'java'
        """

        when: "task is run"
        BuildResult result = runner
                .withArguments('build')
                .build()

        then: "task succeeds"
        result.task(":build").outcome == TaskOutcome.SUCCESS

        and: "jar exists"
        def outJar = testProjectDir.root.toPath().resolve("build/libs").toFile().listFiles()
                .find { file -> file.name.endsWith("1.2.3.jar") }
        outJar.exists()
    }
}
