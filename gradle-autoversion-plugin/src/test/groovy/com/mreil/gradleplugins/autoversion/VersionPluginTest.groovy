package com.mreil.gradleplugins.autoversion

import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.ProjectConfigurationException
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

class VersionPluginTest extends Specification {
    @Rule
    final TemporaryFolder testProjectDir = new TemporaryFolder()

    @Shared
    Project project

    def setup() {
        project = ProjectBuilder.builder()
                .withProjectDir(testProjectDir.root)
                .build()
        project.plugins.apply("java")
        project.plugins.apply(VersionPlugin)
    }

    def "Plugin was applied"() {
        expect: "plugin is in plugins"
        project.plugins.findPlugin(VersionPlugin)
    }

    def "Default mode"() {
        expect: "mode is RELEASE"
        getExtension().mode == "RELEASE"
    }

    def "Set mode"() {
        when: "mode is set"
        getExtension().mode("continuous")

        then: "mode is persisted"
        getExtension().mode == "CONTINUOUS"
    }

    def "Set invalid mode"() {
        when: "mode is set"
        getExtension().mode("bla")

        then: "mode is persisted"
        GradleException e = thrown()
        e.message == "Mode must be one of: [RELEASE, CONTINUOUS]"
    }

    def "Default properties file is set if exists"() {
        when: "default props file exists"
        testProjectDir.newFile("version.properties") << "version=1.0.0"

        and: "plugin is evaluated"
        project.evaluate()

        then:
        getExtension().versionPropertyFile
    }

    def "Default properties file is not set if not exists"() {
        when: "plugin is evaluated"
        project.evaluate()

        then:
        getExtension().versionPropertyFile == null
    }

    def "Error if specified properties file does not exist"() {
        when: "non-existant file is specified"
        getExtension().versionPropertyFile = new File("somefile")

        and: "project is evaluated"
        project.evaluate()

        then:
        ProjectConfigurationException ex = thrown()
        ex.cause.message == "File does not exist: somefile"
    }

    def "Error if specified properties file does not contain version property"() {
        when: "default props file exists"
        testProjectDir.newFile("version.properties") << "bla=1.0.0"

        and: "plugin is evaluated"
        project.evaluate()

        then:
        ProjectConfigurationException ex = thrown()
        ex.cause.message.matches "File .*version\\.properties does not contain property: version"
    }

    def "Version cannot specified in both places"() {
        given: "A project version was set"
        project.version = "1.2.3"

        and: "a version was set in the props file"
        testProjectDir.newFile("version.properties") << "version=1.0.0"

        when: "project is evaluated"
        project.evaluate()

        then: "error"
        ProjectConfigurationException ex = thrown()
        ex.cause.message == "Version cannot be specified in properties file and in project."
    }

    def "Version in props becomes project version"() {
        given: "a version was set in the props file"
        testProjectDir.newFile("version.properties") << "version=1.2.3"

        when: "project is evaluated"
        project.evaluate()

        then: "project version is set"
        project.version == "1.2.3"
    }

    def "Project version is picked up"() {
        given: "A version was set"
        project.version = "version"

        when: "project is evaluated"
        project.evaluate()

        then: "version is specified"
        getExtension().specified

        and: "original version is reported"
        getExtension().originalVersion == project.version
    }

    private VersionPluginExtension getExtension() {
        project.extensions.getByName(VersionPluginExtension.NAME) as VersionPluginExtension
    }

    def "SemVer is parsed"() {
        given: "A semantic version was set"
        project.version = "1.2.3"

        when: "project is evaluated"
        project.evaluate()

        then: "version is specified"
        getExtension().semVer.isPresent()
    }

    def "Invalid SemVer is ignored"() {
        given: "A semantic version was set"
        project.version = "meh"

        when: "project is evaluated"
        project.evaluate()

        then: "semver is empty"
        !getExtension().semVer.isPresent()
    }

    def "Snapshot is parsed"() {
        given: "A snapshot version was set"
        project.version = "1.2.3-SNAPSHOT"

        when: "project is evaluated"
        project.evaluate()

        then: "version is specified"
        getExtension().semVer.isPresent()
        getExtension().semVer.get().preReleaseVersion == "SNAPSHOT"
        getExtension().isSnapshot()
    }
}
