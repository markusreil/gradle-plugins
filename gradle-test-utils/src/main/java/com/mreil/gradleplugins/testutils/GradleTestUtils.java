package com.mreil.gradleplugins.testutils;

import org.gradle.api.Project;
import org.gradle.api.Task;

public class GradleTestUtils {
    public static Task runTask(Project project, String taskName) {
        Task task = project.getTasks().findByName(taskName);
        task.getActions().forEach( a -> a.execute(task));
        return task;
    }
}
